package ru.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IActiveMQConnectionService {

    void receive(@NotNull final MessageListener listener);

}

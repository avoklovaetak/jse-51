package ru.volkova.tm.service;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IActiveMQConnectionService;

import static ru.volkova.tm.constant.ActiveMQConst.*;

import javax.jms.*;

public class ActiveMQConnectionService implements IActiveMQConnectionService {

    @NotNull
    private final ConnectionFactory connectionFactory;

    public ActiveMQConnectionService() {
        connectionFactory = new ActiveMQConnectionFactory(URL);
    }

    @Override
    @SneakyThrows
    public void receive(@NotNull final MessageListener listener) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(STRING);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(listener);
    }

}

package ru.volkova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

public class UserLogoutCommand extends AbstractAuthCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "log out of system";
    }

    @Override
    public void execute() {
        if (bootstrap == null) throw new ObjectNotFoundException();
        if (endpointLocator == null) throw new ObjectNotFoundException();
        @Nullable final Session session = bootstrap.getSession();
        System.out.println("[LOGOUT]");
        endpointLocator.getSessionEndpoint().closeSession(session);
    }

    @NotNull
    @Override
    public String name() {
        return "user-logout";
    }

}


package ru.volkova.tm.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for projectGraph complex type.
 * 
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * 
 * &lt;pre&gt;
 * &amp;lt;complexType name="projectGraph"&amp;gt;
 *   &amp;lt;complexContent&amp;gt;
 *     &amp;lt;extension base="{http://endpoint.tm.volkova.ru/}abstractOwnerEntityGraph"&amp;gt;
 *       &amp;lt;sequence&amp;gt;
 *         &amp;lt;element name="taskGraphs" type="{http://endpoint.tm.volkova.ru/}taskGraph" maxOccurs="unbounded" minOccurs="0"/&amp;gt;
 *       &amp;lt;/sequence&amp;gt;
 *     &amp;lt;/extension&amp;gt;
 *   &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "projectGraph", propOrder = {
    "taskGraphs"
})
public class ProjectGraph
    extends AbstractOwnerEntityGraph
{

    @XmlElement(nillable = true)
    protected List<TaskGraph> taskGraphs;

    /**
     * Gets the value of the taskGraphs property.
     * 
     * &lt;p&gt;
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a &lt;CODE&gt;set&lt;/CODE&gt; method for the taskGraphs property.
     * 
     * &lt;p&gt;
     * For example, to add a new item, do as follows:
     * &lt;pre&gt;
     *    getTaskGraphs().add(newItem);
     * &lt;/pre&gt;
     * 
     * 
     * &lt;p&gt;
     * Objects of the following type(s) are allowed in the list
     * {@link TaskGraph }
     * 
     * 
     */
    public List<TaskGraph> getTaskGraphs() {
        if (taskGraphs == null) {
            taskGraphs = new ArrayList<TaskGraph>();
        }
        return this.taskGraphs;
    }

}

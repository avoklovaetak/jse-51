package ru.volkova.tm.api.service.model;

import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.model.AbstractEntityGraph;

public interface IServiceGraph<E extends AbstractEntityGraph> extends IRepositoryGraph<E> {

}

package ru.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.model.UserGraph;

import java.util.List;

public interface IUserRepositoryGraph extends IRepositoryGraph<UserGraph> {

    @NotNull
    UserGraph insert(@Nullable UserGraph userGraph);

    void clear();

    @NotNull
    List<UserGraph> findAll();

    @Nullable
    UserGraph findByEmail(@NotNull String email);

    @Nullable
    UserGraph findById(@NotNull String id);

    @Nullable
    UserGraph findByLogin(@NotNull String login);

    void lockByEmail(@NotNull String email);

    void lockById(@NotNull String id);

    void lockByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    void unlockByEmail(@NotNull String email);

    void unlockById(@NotNull String id);

    void setPassword(
            @NotNull final String userId,
            @Nullable final String hash
    );

    void unlockByLogin(@NotNull String login);

    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}

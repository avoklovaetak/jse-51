package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.service.IConnectionService;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.ServiceLocator;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.model.UserGraph;
import ru.volkova.tm.repository.dto.SessionRepository;
import ru.volkova.tm.repository.dto.UserRepository;
import ru.volkova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    public void add(@NotNull Session session) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager) ;
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final UserRepository userRepository = new UserRepository(entityManager);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) return null;
            @NotNull final Session session = new Session();
            session.setUser(user);
            @Nullable final Session signSession = sign(session);
            if (signSession == null) return null;
            @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager) ;
            sessionRepository.add(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserGraph userGraph = serviceLocator.getAdminUserService().findByLogin(login);
        if (userGraph == null) return false;
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        final String passwordHash2 = userGraph.getPasswordHash();
        return passwordHash.equals(passwordHash2);
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        final String signature = session.getSignature();
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final Session session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @Nullable UserGraph userGraph = serviceLocator.getAdminUserService().findById(session.getUser().getId());
        if (userGraph == null) throw new AccessDeniedException();
        if (userGraph.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

    @Override
    @Nullable
    public Session close(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final SessionRepository sessionRepository = new SessionRepository(entityManager) ;
            sessionRepository.close(session);
            entityManager.getTransaction().commit();
            return session;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @Nullable
    public List<Session> findAll() {
        return null;
    }

    @Nullable
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

}
